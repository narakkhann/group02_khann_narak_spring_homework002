package com.example.homework002_spring.controller;
import com.example.homework002_spring.model.entity.Product;
import com.example.homework002_spring.model.request.ProductRequest;
import com.example.homework002_spring.model.response.ApiResponse;
import com.example.homework002_spring.service.ProductService;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@RestController
@RequestMapping("/api/v1/products")
public class ProductController {
    private final ProductService productService;
    public ProductController(ProductService productService) {
        this.productService = productService;
    }
    @GetMapping("/all")
    public ResponseEntity<ApiResponse<List<Product>>> getAllProduct(){
        ApiResponse<List<Product>> response= ApiResponse.<List<Product>>builder()
                .message("This product was successfully")
                .payload(productService.getAllProduct())
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }
    @PostMapping()
    public ResponseEntity<?> addNewProduct(@RequestBody ProductRequest productRequest){
        ApiResponse<Product> response = new ApiResponse<>(
                productService.addNewProduct(productRequest),
                "This product add successfully",
                true
        );
        return ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Integer productId){
        ApiResponse<Product> response = new ApiResponse<>(
                productService.getById(productId),
                "This product find successfully",
                true
        );
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProductById(@PathVariable("id") Integer productId){
        productService.deleteProductById(productId);
        ApiResponse<Product> response = new ApiResponse<>(
                null,
                "This product deelte successfully",
                true
        );
        return ResponseEntity.ok(response);
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> updateProductById(@PathVariable("id") Integer productId,@RequestBody ProductRequest productRequest){
        productService.updateProductById(productId,productRequest);
        return ResponseEntity.ok(new ApiResponse<>(
                null,
                "This product was update successfully",
                true
        ));
    }
}
