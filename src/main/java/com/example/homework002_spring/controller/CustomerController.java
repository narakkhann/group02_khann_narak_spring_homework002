package com.example.homework002_spring.controller;

import com.example.homework002_spring.model.entity.Customer;
import com.example.homework002_spring.model.entity.Product;
import com.example.homework002_spring.model.request.CustomerRequest;
import com.example.homework002_spring.model.request.ProductRequest;
import com.example.homework002_spring.model.response.ApiResponse;
import com.example.homework002_spring.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
    @GetMapping("/all")
    public ResponseEntity<List<Customer>> getAllCustomer(){
        return ResponseEntity.ok(customerService.getAllCustomer());
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable("id") Integer customerId){
        ApiResponse<Customer> response = new ApiResponse<>(
                customerService.getById(customerId),
                "This customer was find successfully",
                true
        );
        return ResponseEntity.ok(response);
    }
    @PostMapping("/customer")
    public ResponseEntity<?> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        ApiResponse<Customer> response = new ApiResponse<>(
                customerService.addNewCustomer(customerRequest),
                "This customer was add successfully",
                true
        );
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> DeleteCustomer(@PathVariable("id") Integer customerId){
        customerService.deleteCustomerById(customerId);
        ApiResponse<Customer> response = new ApiResponse<>(
                null,
                "This customer was delete successfully",
                true
        );
        return ResponseEntity.ok(response);
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable("id")Integer customerId,@RequestBody CustomerRequest customerRequest){
        customerService.updateCustomerById(customerId,customerRequest);
        return ResponseEntity.ok(new ApiResponse<>(
                null,
                "This customer was update successfully",
                true
        ));
    }
}
