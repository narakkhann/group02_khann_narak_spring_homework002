package com.example.homework002_spring.controller;

import com.example.homework002_spring.model.entity.Invoice;
import com.example.homework002_spring.model.request.InvoiceRequest;
import com.example.homework002_spring.model.request.ProductRequest;
import com.example.homework002_spring.model.response.ApiResponse;
import com.example.homework002_spring.service.InvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@RestController
@RequestMapping("/api/v1/invoices")
public class InvoiceController {
    private  final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }
    @GetMapping("/all")
    public ResponseEntity<List<Invoice>> getAllInvoices(){

        return ResponseEntity.ok(invoiceService.getAllInvoices());
    }
    @PostMapping("/invoice")
    public ResponseEntity<?> insertInvoice(@RequestBody InvoiceRequest invoiceRequest){
        invoiceService.insertNewProduct(invoiceRequest);
        return ResponseEntity.ok(
                new ApiResponse<List<Invoice>>(
                        null,
                        "Insert successfully",
                        true
                )
        );
    }
}
