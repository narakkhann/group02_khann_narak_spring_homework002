package com.example.homework002_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homework002SpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(Homework002SpringApplication.class, args);
    }

}
