package com.example.homework002_spring.repository;

import com.example.homework002_spring.model.entity.Product;
import com.example.homework002_spring.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("SELECT * FROM products")
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "name")
    @Result(property = "productPrice",column = "price")
    List<Product> findAllProduct();
    @Select("SELECT * FROM products WHERE product_id=#{productId}")
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "name")
    @Result(property = "productPrice",column = "price")
    Product findById(Integer productId);
    @Delete("DELETE FROM products WHERE product_id=#{id}")
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "name")
    @Result(property = "productPrice",column = "price")
    boolean DeleteById(@Param("id") Integer productId);
    @Select("""
            INSERT INTO products(name,price)
            VALUES(#{pro.productName},#{pro.productPrice})
            RETURNING *
            """)
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "name")
    @Result(property = "productPrice",column = "price")
    Product saveProduct(@Param("pro") ProductRequest productRequest);
    @Select("""
            UPDATE products 
            SET name=#{pro.productName}, price=#{pro.productPrice}
            WHERE product_id=#{productId}
            """)
    Product updateProduct(Integer productId,@Param("pro")ProductRequest productRequest);
    @Select("""
                    SELECT p.product_id,p.name,p.price FROM products p
                    INNER JOIN invoice_detail_tb inv
                    ON p.product_id = inv.product_id WHERE invoice_id = #{id};
             """)
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "name")
    @Result(property = "productPrice",column = "price")

    List<Product> getProductByInvoiceId(@Param("id") Integer invoiceId);
}
