package com.example.homework002_spring.repository;

import com.example.homework002_spring.model.entity.Customer;
import com.example.homework002_spring.model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    @Select("SELECT * FROM Customers")
    List<Customer> findAllCustomer();
    @Select("SELECT * FROM customers WHERE customer_id=#{customerId}")
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    Customer findCustomerById(Integer customerId);
    @Select("""
    INSERT INTO customers (customer_name,customer_address,customer_phone)
    VALUES(#{customerName}, #{customerAddress}, #{customerPhone})
    RETURNING *
    """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    Customer insertNewCustomer(CustomerRequest customerRequest);
    @Delete("""
            DELETE FROM customers WHERE customer_id = #{id};
            """)
    boolean deleteCustomer(@Param("id") Integer customerId);
    @Select("""
            UPDATE customers 
            SET customer_name = #{cus.customerName},customer_address = #{cus.customerAddress},customer_phone = #{cus.customerPhone}
            WHERE customer_id=#{customerId}
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    Customer updateCustomer(Integer customerId,@Param("cus")CustomerRequest customerRequest);
}
