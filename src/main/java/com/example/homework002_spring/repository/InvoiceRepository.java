package com.example.homework002_spring.repository;

import com.example.homework002_spring.model.entity.Invoice;
import com.example.homework002_spring.model.entity.Product;
import com.example.homework002_spring.model.request.InvoiceRequest;
import jdk.jfr.Timestamp;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {
    @Select("SELECT * FROM invoice_db")
    @Result(property = "invoiceId",column = "invoice_id")
    @Result(property = "invoiceDate",column = "invoice_date")
    @Result(property = "customer",column = "customer_id",
            one = @One(select = "com.example.homework002_spring.repository.CustomerRepository.findCustomerById"
            )
    )
    @Result(property = "products",column="invoice_id",
            many = @Many(select="com.example.homework002_spring.repository.ProductRepository.getProductByInvoiceId")
    )
    List<Invoice> findAllInvoices();
    @Select("""
            INSERT INTO invoice_db (customer_id)
            VALUES(#{invoice.customerId})
            RETURNING invoice_id;
            """)
    public Integer insertInvoice(@Param("invoice")InvoiceRequest invoiceRequest);
    @Insert("""
            INSERT INTO invoice_detail_tb(invoice_id,product_id)
            VALUES(#{invoiceId}, #{productId})
            """)
    public void insertIntoInvoiceDetail(Integer invoiceId, Integer productId);
}
