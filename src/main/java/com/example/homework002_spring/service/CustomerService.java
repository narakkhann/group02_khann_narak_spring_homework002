package com.example.homework002_spring.service;

import com.example.homework002_spring.model.entity.Customer;
import com.example.homework002_spring.model.request.CustomerRequest;
import org.springframework.stereotype.Service;

import java.util.List;
public interface CustomerService {
    List<Customer> getAllCustomer();
    Customer getById(Integer customerId);
    Customer addNewCustomer(CustomerRequest customerRequest);
    boolean deleteCustomerById(Integer customerId);
    Customer updateCustomerById(Integer customerId,CustomerRequest customerRequest);
}
