package com.example.homework002_spring.service;

import com.example.homework002_spring.model.entity.Product;
import com.example.homework002_spring.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();
    Product getById(Integer productId);
    boolean deleteProductById(Integer productId);
    Product addNewProduct(ProductRequest productRequest);
    Product updateProductById(Integer productId, ProductRequest productRequest);
}
