package com.example.homework002_spring.service.serviceImp;

import com.example.homework002_spring.model.entity.Product;
import com.example.homework002_spring.model.request.ProductRequest;
import com.example.homework002_spring.repository.ProductRepository;
import com.example.homework002_spring.service.ProductService;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product getById(Integer productId) {

        return productRepository.findById(productId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {

        return productRepository.DeleteById(productId);
    }

    @Override
    public Product addNewProduct(ProductRequest productRequest) {
        return productRepository.saveProduct(productRequest);
    }

    @Override
    public Product updateProductById(Integer productId, ProductRequest productRequest) {
        return productRepository.updateProduct(productId,productRequest);
    }

//    @Override
//    public Product updateProductById() {
//        return null;
//    }
}
