package com.example.homework002_spring.service.serviceImp;

import com.example.homework002_spring.model.entity.Customer;
import com.example.homework002_spring.model.request.CustomerRequest;
import com.example.homework002_spring.repository.CustomerRepository;
import com.example.homework002_spring.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getById(Integer customerId) {
        return customerRepository.findCustomerById(customerId);
    }

    @Override
    public Customer addNewCustomer(CustomerRequest customerRequest) {
        return customerRepository.insertNewCustomer(customerRequest);
    }

    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomer(customerId);
    }

    @Override
    public Customer updateCustomerById(Integer customerId, CustomerRequest customerRequest) {
        return customerRepository.updateCustomer(customerId,customerRequest);
    }
}
