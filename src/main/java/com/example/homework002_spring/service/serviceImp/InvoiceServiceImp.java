package com.example.homework002_spring.service.serviceImp;

import com.example.homework002_spring.model.entity.Invoice;
import com.example.homework002_spring.model.request.InvoiceRequest;
import com.example.homework002_spring.repository.InvoiceRepository;
import com.example.homework002_spring.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InvoiceServiceImp implements InvoiceService {
    private final InvoiceRepository invoiceRepository;
    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }
    @Override
    public List<Invoice> getAllInvoices() {
        return invoiceRepository.findAllInvoices();
    }

    @Override
    public void insertNewProduct(InvoiceRequest invoiceRequest) {
        Integer invoiceId = invoiceRepository.insertInvoice(invoiceRequest);
        for (Integer productId : invoiceRequest.getProductId()){
            invoiceRepository.insertIntoInvoiceDetail(invoiceId, productId);
        }
    }


}
