package com.example.homework002_spring.service;

import com.example.homework002_spring.model.entity.Invoice;
import com.example.homework002_spring.model.request.InvoiceRequest;
import org.springframework.stereotype.Service;

import java.util.List;
public interface InvoiceService {
    List<Invoice> getAllInvoices();
    void insertNewProduct(InvoiceRequest invoiceRequest);
}
