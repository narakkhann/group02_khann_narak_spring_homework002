CREATE TABLE products(
    product_id SERIAL PRIMARY KEY ,
    name VARCHAR(255),
    price DECIMAL(7,2)
);
CREATE TABLE customers(
    customer_id SERIAL PRIMARY KEY ,
    customer_name VARCHAR(255),
    customer_address VARCHAR(255),
    customer_phone VARCHAR(20)
);
CREATE TABLE invoice_db(
    invoice_id SERIAL PRIMARY KEY,
    invoice_Date TIMESTAMP,
    customer_id INT REFERENCES customers(customer_id)
);
CREATE TABLE invoice_detail_tb(
    id SERIAL PRIMARY KEY ,
    invoice_id INT REFERENCES invoice_db(invoice_id) ON UPDATE CASCADE ON DELETE CASCADE,
    product_id INT REFERENCES products(product_id) ON UPDATE CASCADE ON DELETE CASCADE
)